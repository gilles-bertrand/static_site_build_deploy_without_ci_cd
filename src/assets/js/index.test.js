import { TestScheduler } from 'jest';
import { checkCredentials } from './index';
test('Incorrect checkCredentials', () => {
    expect(checkCredentials('gilles', 'test')).toBe(-1)
});

test('correct checkCredentials', () => {
    expect(checkCredentials('joelle', 'password')).toBe(1)
})