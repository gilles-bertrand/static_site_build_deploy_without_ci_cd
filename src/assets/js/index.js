const authorizedUsers = [
    {name:"gilles", password:"password"},
    {name:"joelle", password:"password"}

]
const init = ()=>{
    createInteractive();
}
const createInteractive = ()=> {
   const loginForm= document.querySelector('form');
   loginForm.addEventListener('submit',(event)=>{event.preventDefault();login(loginForm.elements);})
}

const checkCredentials = (login, password)=>{
    return authorizedUsers.findIndex(item=>item.name === login && item.password === password);
}


const login = ({login,password})=>{
    console.log(checkCredentials(login.value, password.value))
    if(checkCredentials(login.value, password.value) === -1){
        alert("Your credentials are wrong")
    } else {
        window.location.href="/internal-page.html";
    }
    
}
window.addEventListener("load",()=>{init()})

export {checkCredentials, login}